#!/usr/bin/env python

from plumbum import cli, local, BG
from plumbum.cmd import postgres

class ScriptApp(cli.Application):
    def main(self, *args):
        if args:
            print "Unknown command:", args
            return 1

        if not self.nested_command:
            print "No command given"
            return 1

@ScriptApp.subcommand("launchdb")
class LaunchDB(cli.Application):
    data_dir = cli.SwitchAttr("--data-dir", str, help="The postgres data directory", default="/var/data")

    def main(self):
        with local.as_user('postgres'):
            print (postgres['-D', self.data_dir]) & BG


if __name__ == "__main__":
    ScriptApp.run()
