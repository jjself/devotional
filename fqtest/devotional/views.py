# Create your views here.
from django.views.generic import DetailView
from django.shortcuts import get_object_or_404, render
from django.utils.html import strip_tags

from .models import Devotional

class DevotionalDetailView(DetailView):
    context_object_name = 'devotional'

    def get_object(self):
        return get_object_or_404(Devotional,
                                 month = self.kwargs['month'],
                                 day = self.kwargs['day'])

def count(request):
    devotions = Devotional.objects.all()
    words = 0

    qs = Devotional.objects.all()
    for devotional in qs:
        body = strip_tags(devotional.body)
        count = len(body.split(' '))
        words += count
        devotional.count = count

    return render(request, 'devotional/count.html', {'count':words, 'devotionals':qs})
