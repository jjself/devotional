from django.db import models
from django.core.urlresolvers import reverse
from django.contrib import admin

from taggit.managers import TaggableManager
# Create your models here.

class Devotional(models.Model):
    title = models.CharField(max_length=255)
    month = models.IntegerField()
    day = models.IntegerField()
    body = models.TextField()

    tags = TaggableManager(TaggableManager)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('devotional-detail', args=(self.month, self.day))

admin.site.register(Devotional)
