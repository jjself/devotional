import datetime
import logging
import csv
import HTMLParser

from django.core.management.base import BaseCommand, CommandError

from fqtest.devotional.models import Devotional

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())

class Command(BaseCommand):
    args = "import_file"
    help = "Imports a CSV file for the daily devotional"

    def handle(self, *args, **kwargs):

        if not args:
            raise CommandError("Import file is required")

        try:
            with open(args[0]) as input_file:
                reader = csv.DictReader(input_file, skipinitialspace=True)
                for item in reader:
                    try:
                        logger.info("Starting import for %s", item['title'])
                        #Note that day and month are backwards in the CSV sample
                        month = int(item['day'])
                        day = int(item['month'])

                        #unescape html
                        html_parser = HTMLParser.HTMLParser()
                        body = html_parser.unescape(item['body'])

                        #validate the data and that the day matches the month
                        date = datetime.date(year=datetime.date.today().year,
                                             month = month,
                                             day = day)

                        d, is_new = Devotional.objects.get_or_create(title=item['title'],
                                                                     month=month,
                                                                     day=day,
                                                                     defaults={'body':body})
                        if is_new:
                            logger.info("Created new devotional for %s", item['title'])
                        else:
                            logger.info("Devotional already exists for %s, skipping", item['title'])
                    except Exception, e:
                        #don't want to hard fail on error here, log it and continue to next item
                        logger.exception("Error creating item for %s", item['title'])

        except IOError:
            logger.exception("Unable to open file")
            raise CommandError("Not able to open file %s"%args[0])
