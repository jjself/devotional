from django.conf.urls import patterns, include, url

from .views import DevotionalDetailView


urlpatterns = patterns('fqtest.devotional.views',
    # url(r'^$', 'fqtest.views.home', name='home'),
     url(r'^(?P<month>\d+)/(?P<day>\d+)/$', DevotionalDetailView.as_view(), name='devotional-detail'),
     url(r'^count/$', 'count')
)
